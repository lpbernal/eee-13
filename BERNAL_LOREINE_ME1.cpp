#include <iostream>
#include <math.h>
using namespace std; 
int main ()
{
    int n, counter, gcn; 
    cin >> n;
   for (counter=0; counter<=((pow(2,n))-1); counter++) { 
            gcn = (counter^(counter>>1));
            cout << gcn <<endl;
        }
    return 0;
}

/*references: https://www.youtube.com/watch?v=cF-Q5j7RUEw, https://code.tutsplus.com/articles/understanding-bitwise-operators--active-11301, 
https://www.tutorialspoint.com/cplusplus/cpp_bitwise_operators.htm, https://www.geeksforgeeks.org/decimal-equivalent-gray-code-inverse/ */
